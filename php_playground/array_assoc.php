<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    
    <?php
    
    $number = [20, 45, 32];
    echo "Array number is: ";
    print_r($number);
    
    echo "<br>" . $number[2] . "<br>";
    echo $number[0] . "<br>". "<br>";
    
    $name = ["Hicham", "Punpun", "Quentin"];
    echo "Array name is: ";
    print_r($name);
    
    
    // assoc array
    $name_assoc = ["first_name" => 'Harry', "middle_name" => 'James', "last_name" => 'Potter'];
    echo  "<br>" . "<br>" . "Array name_assoc is: " . "<br>";
    print_r($name_assoc);
    ?>
</body>
</html>