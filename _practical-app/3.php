<?php include "functions.php"; ?>
<?php include "includes/header.php";?>

	<section class="content">

	<aside class="col-xs-4">

	<?php Navigation();?>
			
	</aside><!--SIDEBAR-->


<article class="main-content col-xs-8">

<?php  

//  Step1: Make an if Statement with elseif and else to finally display string saying, I love PHP
    
    if(6 < 2){
        echo "I love Javascript";
    }else if(5 == 4){
        echo "I love Docker";
    }else if(5 != 5){
        echo "I love Docker";
    }else if(5 == "5"){
        echo "I love PHP" . "<br>";
    }



	// Step 2: Make a forloop  that displays 10 numbers
    for($count = 0; $count < 10; $count++){
        echo $count + 1 . "<br>";
    }


	// Step 3 : Make a switch Statement that test againts one condition with 5 cases
    
    $number = 5;
    switch($number){
        case 1:
            echo "bukan";
            break;
        case 2:
            echo "bukan";
            break;
        case 3:
            echo "bukan";
            break;
        case 4:
            echo "bukan";
            break;
        case 5:
            echo "ya betul";
            break;
    }

?>






</article><!--MAIN CONTENT-->
	
<?php include "includes/footer.php"; ?>